package com.example.ticker.unit;

import com.example.ticker.business.CsvService;
import com.example.ticker.business.Parser;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

public class CsvServiceUnitTest {
    private static final String PARSED_LINE = "PARSED LINE";

    @Mock
    private Parser<String> parser;

    private CsvService<String> subject;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        subject = new CsvService<>();

        doReturn(PARSED_LINE).when(parser).parse(Mockito.anyString());
    }

    @Test
    public void parse_whenNoDataSpecified_returnsEmptyList() {
        String data = "";
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());

        List<String> parsedResults = subject.parse(inputStream, parser);

        assertThat(parsedResults, Matchers.is(List.of()));
    }

    @Test
    public void parse_whenSingleLineSpecified_skipsThatLineAndReturnsEmptyList() {
        String data = "Dummy String";
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());

        List<String> parsedResults = subject.parse(inputStream, parser);

        assertThat(parsedResults, Matchers.is(List.of()));
    }

    @Test
    public void parse_withMultipleLines_returnsLinesAfterTheFirst() {
        String data = "Dummy String\nActual Line";
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());

        List<String> parsedResults = subject.parse(inputStream, parser);

        assertThat(parsedResults, Matchers.is(List.of(PARSED_LINE)));
    }

    @Test
    public void parse_whenExceptionEncounteredParsingLine_skipsThatLine() throws Exception {
        doThrow(ParseException.class).when(parser).parse(Mockito.anyString());

        String data = "Dummy String\nActual Line";
        InputStream inputStream = new ByteArrayInputStream(data.getBytes());

        List<String> parsedResults = subject.parse(inputStream, parser);

        assertThat(parsedResults, Matchers.is(List.of()));
    }
}
