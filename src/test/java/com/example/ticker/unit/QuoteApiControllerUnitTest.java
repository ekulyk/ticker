package com.example.ticker.unit;

import com.example.ticker.api.QuoteApiController;
import com.example.ticker.persistence.Quote;
import com.example.ticker.persistence.QuoteRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.doReturn;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class QuoteApiControllerUnitTest {

    @Mock
    private QuoteRepository quoteRepository;

    private QuoteApiController subject;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        subject = new QuoteApiController(quoteRepository);
    }

    @Test
    public void get_quotes_whenFilterIsNull_returnsAllItems() {
        List<Quote> allItems = List.of(new Quote());

        doReturn(allItems).when(quoteRepository).findAll();

        Iterable<Quote> quotes = subject.getQuotes(null);

        assertThat(quotes, is(allItems));
    }

    @Test
    public void get_quotes_whenNoFilterIsEmpty_returnsAllItems() {
        List<Quote> allItems = List.of(new Quote());

        doReturn(allItems).when(quoteRepository).findAll();

        Iterable<Quote> quotes = subject.getQuotes("");

        assertThat(quotes, is(allItems));
    }

    @Test
    public void get_quotes_whenFilterSpecified_returnsItemsForThatFilter() throws Exception {
        List<Quote> filteredItems = List.of(new Quote());

        doReturn(filteredItems).when(quoteRepository).findByStock("STOCK");

        Iterable<Quote> quotes = subject.getQuotes("STOCK");

        assertThat(quotes, is(filteredItems));
    }
}
