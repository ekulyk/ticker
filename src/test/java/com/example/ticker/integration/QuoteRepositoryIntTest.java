package com.example.ticker.integration;

import com.example.ticker.persistence.Quote;
import com.example.ticker.persistence.QuoteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
public class QuoteRepositoryIntTest {
    @Autowired
    private QuoteRepository subject;

    @Test
    public void findByStock_whenQuoteWithStockExists_ableToFindThatQuote() {
        makeQuote("TICKER", new Date());

        List<Quote> quotes = subject.findByStock("TICKER");

        assertThat(quotes, iterableWithSize(1));
    }

    @Test
    public void findByStock_whenQuoteWithStockThatDoesNotMatchCase_notAbleToFindThatQuote() {
        makeQuote("TICKER", new Date());

        List<Quote> quotes = subject.findByStock("tICker");

        assertThat(quotes, is(empty()));
    }

    @Test
    public void findByStock_whenQuoteWithStockThatDoesNotExist_returnsEmptyList() {
        List<Quote> quotes = subject.findByStock("TICKER");

        assertThat(quotes, is(empty()));
    }

    @Test
    public void create_whenGivenDuplicateRecords_willPersistBoth() {
        Date date = new Date();
        makeQuote("TICKER", date);
        makeQuote("TICKER", date);

        Iterable<Quote> quotes = subject.findAll();

        assertThat(quotes, iterableWithSize(2));
    }

    private void makeQuote(String stock, Date date) {
        Quote quote = new Quote(1,
                stock,
                date,
                1.0,
                1.0,
                1.0,
                1.0, 1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0);
        subject.save(quote);
    }
}
