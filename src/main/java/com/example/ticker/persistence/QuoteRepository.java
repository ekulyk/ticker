package com.example.ticker.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuoteRepository extends CrudRepository<Quote, Long> {
    List<Quote> findByStock(String stock);
}
