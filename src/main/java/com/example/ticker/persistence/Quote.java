package com.example.ticker.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Quote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int quarter;
    private String stock;
    private Date date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Double volume;
    private Double percentChangePrice;
    private Double percentChangeVolumeOverLastWeek;
    private Double previousWeeksVolume;
    private Double nextWeeksOpen;
    private Double nextWeeksClose;
    private Double percentChangeNextWeeksPrice;
    private Double daysToNextDividend;
    private Double percentReturnNextDividend;

    public Quote() {};

    public Quote(int quarter,
                 String stock,
                 Date date,
                 Double open,
                 Double high,
                 Double low,
                 Double close,
                 Double volume,
                 Double percentChangePrice,
                 Double percentChangeVolumeOverLastWeek,
                 Double previousWeeksVolume,
                 Double nextWeeksOpen,
                 Double nextWeeksClose,
                 Double percentChangeNextWeeksPrice,
                 Double daysToNextDividend,
                 Double percentReturnNextDividend) {
        this.quarter = quarter;
        this.stock = stock;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.percentChangePrice = percentChangePrice;
        this.percentChangeVolumeOverLastWeek = percentChangeVolumeOverLastWeek;
        this.previousWeeksVolume = previousWeeksVolume;
        this.nextWeeksOpen = nextWeeksOpen;
        this.nextWeeksClose = nextWeeksClose;
        this.percentChangeNextWeeksPrice = percentChangeNextWeeksPrice;
        this.daysToNextDividend = daysToNextDividend;
        this.percentReturnNextDividend = percentReturnNextDividend;
    }

    public int getQuarter() {
        return quarter;
    }

    public String getStock() {
        return stock;
    }

    public Date getDate() {
        return date;
    }

    public Double getOpen() {
        return open;
    }

    public Double getHigh() {
        return high;
    }

    public Double getLow() {
        return low;
    }

    public Double getClose() {
        return close;
    }

    public Double getVolume() {
        return volume;
    }

    public Double getPercentChangePrice() {
        return percentChangePrice;
    }

    public Double getPercentChangeVolumeOverLastWeek() {
        return percentChangeVolumeOverLastWeek;
    }

    public Double getPreviousWeeksVolume() {
        return previousWeeksVolume;
    }

    public Double getNextWeeksOpen() {
        return nextWeeksOpen;
    }

    public Double getNextWeeksClose() {
        return nextWeeksClose;
    }

    public Double getPercentChangeNextWeeksPrice() {
        return percentChangeNextWeeksPrice;
    }

    public Double getDaysToNextDividend() {
        return daysToNextDividend;
    }

    public Double getPercentReturnNextDividend() {
        return percentReturnNextDividend;
    }
}
