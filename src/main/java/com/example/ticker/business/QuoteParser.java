package com.example.ticker.business;

import com.example.ticker.persistence.Quote;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class QuoteParser implements Parser<Quote> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");

    public Quote parse(String csvString) throws ParseException {
        List<String> parsedString = Arrays.asList(csvString.split(","));

        return new Quote(
                parseInt(parsedString.get(0)),
                parsedString.get(1),
                safeParseDate(parsedString.get(2)),
                safeParseMoney(parsedString.get(3)),
                safeParseMoney(parsedString.get(4)),
                safeParseMoney(parsedString.get(5)),
                safeParseMoney(parsedString.get(6)),
                safeParseDouble(parsedString.get(7)),
                safeParseDouble(parsedString.get(8)),
                safeParseDouble(parsedString.get(9)),
                safeParseDouble(parsedString.get(10)),
                safeParseMoney(parsedString.get(11)),
                safeParseMoney(parsedString.get(12)),
                safeParseDouble(parsedString.get(13)),
                safeParseDouble(parsedString.get(14)),
                safeParseDouble(parsedString.get(15))
                );
    }

    private Date safeParseDate(String date) throws ParseException {
        if (date == null || date.isBlank()) {
            return null;
        }

        return DATE_FORMAT.parse(date);
    }

    private Double safeParseMoney(String money) {
        if (money == null || money.isBlank()) {
            return null;
        }

        return parseDouble(money.replace("$", ""));
    }

    private Double safeParseDouble(String number) {
        if (number == null || number.isBlank()) {
            return null;
        }

        return parseDouble(number);
    }
}
