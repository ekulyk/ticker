package com.example.ticker.business;

import java.text.ParseException;

public interface Parser<T> {
    T parse(String value) throws ParseException;
}
