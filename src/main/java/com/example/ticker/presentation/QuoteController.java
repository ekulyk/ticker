package com.example.ticker.presentation;

import com.example.ticker.business.CsvService;
import com.example.ticker.business.QuoteParser;
import com.example.ticker.persistence.Quote;
import com.example.ticker.persistence.QuoteRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class QuoteController {

    private final QuoteRepository quoteRepository;
    private final CsvService<Quote> csvService;

    public QuoteController(QuoteRepository quoteRepository,
                           CsvService<Quote> csvService) {
        this.quoteRepository = quoteRepository;
        this.csvService = csvService;
    }

    @PostMapping("/upload-quote-csv")
    public String uploadQuoteCsv(@RequestParam("file") MultipartFile file, Model model) {
        try {
            List<Quote> quotes = csvService.parse(file.getInputStream(), new QuoteParser());

            quoteRepository.saveAll(quotes);
        } catch (IOException exception) {
            model.addAttribute("message", "An error occurred while processing the file");
        }

        return "";
    }
}
