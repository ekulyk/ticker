package com.example.ticker.api;

import com.example.ticker.persistence.Quote;
import com.example.ticker.persistence.QuoteRepository;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuoteApiController {
    private final QuoteRepository quoteRepository;

    public QuoteApiController(QuoteRepository quoteRepository) {
        this.quoteRepository = quoteRepository;
    }

    @GetMapping("/quotes")
    public Iterable<Quote> getQuotes(@RequestParam(required = false) String stock) {
        if (stock == null || stock.isBlank()) {
            return quoteRepository.findAll();
        }

        return quoteRepository.findByStock(stock);
    }

    @PostMapping("/quotes")
    public Quote createQuote(@RequestBody Quote newQuote) {
        return quoteRepository.save(newQuote);
    }
}
