# Ticker

Simple spring boot application to load and view stock ticker information.

## Description

The application consumes stock ticker information in CSV format from [Dow Jones Index from 2011](http://archive.ics.uci.edu/ml/datasets/Dow+Jones+Index#).
Users are able to upload CSV files to bulk load information, use a REST API to query for information for particular tickers, and add one-off individual records for ticker information.

## Technology

* Spring Boot
* Maven
* JPA
* H2 in-memory Database

## Getting Started

### Executing program

* Run the application using maven
```
./mvnw spring-boot:run
```

* To begin uploading some csv data, navigate to [http://localhost:8080](http://localhost:8080)

### API Examples

* Upload an example csv, some example data
```
curl --form file='@data/dow_jones_index.data' http://localhost:8080/upload-quote-csv
```

* Manually adding a single record
```
curl -X POST -H "Content-Type: application/json" \                                  
    -d '{"quarter":1,"stock":"AA","date":"2021-08-16T21:21:21.406+00:00","open":15.82,"high":16.72,"low":15.78,"close":16.42,"volume":2.39655616E8,"percentChangePrice":3.79267,"percentChangeVolumeOverLastWeek":null,"previousWeeksVolume":null,"nextWeeksOpen":16.71,"nextWeeksClose":15.97,"percentChangeNextWeeksPrice":-4.42849,"daysToNextDividend":26.0,"percentReturnNextDividend":0.182704}' \
    http://localhost:8080/quotes
```

* Get all the quotes available in the system
```
curl http://localhost:8080/quotes
```

* Get all the quotes belonging to a single stock symbol
```
curl http://localhost:8080/quotes?stock=AA
```

## Next Steps

* Bugs
    * Need to properly enforce data correctness
        * missing validation on API input
        * db constraints (example: uniqueness)
    * Exception handling of csv parse errors appropriately and display to user
* Functionality
    * More robust CSV parsing (opencsv or some such)
    * An actual user model
    * Some sort of actual UI
* Diagnostics
    * Logging
    * Metrics
* Performance
    * Large file ingestion and storage
    * Handling large request counts
* Security
    * User access controls

## Authors

Erik Kulyk

